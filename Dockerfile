ARG CT_TAG_DEBIAN
FROM torizon/debian:$CT_TAG_DEBIAN

RUN apt-get update -y && \
    apt-get install --no-install-recommends -y \
        network-manager \
        openssh-server \
        && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN \
    usermod --login netconfig torizon && \
    usermod --home /home/netconfig --move-home netconfig && \
    groupmod --new-name netconfig torizon && \
    echo "netconfig:1234" | chpasswd && \
    usermod --shell /bin/bash netconfig && \
    echo "/usr/bin/nmtui && exit" >> /home/netconfig/.bashrc && \
    mkdir -p /run/sshd && \
    chmod 0755 /run/sshd && \
    ssh-keygen -A

EXPOSE 22
CMD ["/usr/sbin/sshd", "-D"]

