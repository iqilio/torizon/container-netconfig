all: build

build:
	docker build \
		--platform=linux/arm64 \
		--build-arg CT_TAG_DEBIAN="3.3.0" \
		--tag ursiqilio/netconfig \
		.

push: build
	docker login
	docker push \
		ursiqilio/netconfig

run: build
	docker run \
		--platform=linux/arm64 \
		--volume /var/run/dbus:/var/run/dbus \
		--publish 7777:22 \
		--detach \
		--rm \
		ursiqilio/netconfig

